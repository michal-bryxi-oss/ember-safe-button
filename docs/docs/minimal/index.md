---
order: 2
---

# Minimal example

This is a safe button. To trigger the action, user needs to click, wait for the safety to dissapear and then click again.

If the user does not confirm the action in certain time, the safety will roll over the trigger button again to again protect the button from accidental click.
