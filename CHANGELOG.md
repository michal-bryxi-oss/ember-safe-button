# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/2.0.0.html).

## 3.0.0 (2022-08-25)

### Chore

- **💥 BREAKING**: Update to ember addon v2 format using amazing [ember-addon-mirator](https://github.com/NullVoxPopuli/ember-addon-migrator) and help of the folks on Ember Discord.
- **💥 BREAKING**: Tons of package version udpates, including Node version upgrades.


### Changed

- Moved [documentation website](https://ember-safe-button.netlify.app/docs/showcase) from [ember-cli-addon-docs](https://github.com/ember-learn/ember-cli-addon-docs) to [Docfy](https://docfy.dev/docs/ember/).

## 2.1.1 (2021-06-27)

### Chore

- Moved repo to [new location](https://gitlab.com/michal-bryxi/open-source/ember-safe-button)

## 2.1.0 (2021-02-21)

### Added

- `aria-label`s for `safety` dynamically change according to current state of the button, which should give users using assistive technology more hints of what's happening.
### Fixed

- `Safety` is `disabled` only when the component finished animation and is in unlocked state.
- `Trigger` is `disabled` only when safety is not.
- Element in `focus` is always the one that is not `disabled`.
- The three above make sure that the component is never losing focus.

## 2.0.0 (2021-02-19)

### Chagned

- **BREAKING**: Moved from [ember-animated](https://ember-animation.github.io/ember-animated/) to custom usage of [Web Animations API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Animations_API). This changed mostly the internal API of the component, but things might break.

### Added

- New property `@animation`.
   - It either accepts a `string` which then points to one of the [embedded animation patterns](./addon/components/safe-button/animations/index.js).
   - Or it can accept an `object` which is then a series of Web Animation compatible object definitions for _safety_ and _trigger_ _unlocking_ and _locking_ animations.
- Documentation page with showcase of all embedded animations.
- Safety is _only_ clickable when the safety is locked. In all other states (including animation) the button is `disabled`.
- `SafeButton` automatically changes `disabled` and `tabindex` attributes of respective button elements.
- `SafeButton` automatically handles `focus()` of respective button elements.

### Fixed

- Docs app page title is no longer `Dummy`, but `ember-safe-button`.
- First in the DOM we have `trigger` and then `safety` which means that z-index will be in logical way when not explicitly set.

## 1.0.7 (2021-02-07)

### Fixed

- Fixed animations in Safari.

### Changed

- Removed auto-focus from trigger. This caused the Safari to misbehave. Will have to figure out more conceptual fix.
- Replaced ember-animated with ember-animate-css

## 1.0.6 (2019-10-18)

### Fixed

- The addon works when used in external app.

## 1.0.5 (2019-10-18)

### Fixed

- Add ember-animated to dependencies.

## 1.0.4 (2019-10-18)

### Fixed

- Netlify SRI / redirect issues.

## 1.0.3 (2019-10-18)

### Fixed

- Netlify redirects.

## 1.0.2 (2019-10-18)

### Fixed

- Better texts for readme to show what is this addon about.
- Make sure that examples in addon docs work.

### Changed

- Completely remove dependency on tailwind-css even for the docs.
- Add custom styling for the examples.

## 1.0.1 (2019-10-04)

### Fixed

- Correct configuration to generate addon docs.

## 1.0.0 (2019-10-03)

### Changed

- Bumped from Octane preview to ember v3.13.1.

## 0.2.0 (2019-04-06)

### Added

- CHANGELOG (this file :)).
- `aria-label` attributes for better accessibility.
- Example of styling for documentation examples.
- Ability to provide `@message` that renders inside buttons when no block is provided
- Fallback `@message="delete"`

### Changed

- Documentation structure.
- Names of examples in documentation.

## 0.1.0 (2019-03-30)

### Added

- First implementation of `ember-safe-button` that allows basic interation where `safety` dissapears upon click, `trigger` triggers action on click and `safety` gets back after timeout.
- Addon has nice styling through [ember-cli-addon-docs](https://ember-learn.github.io/ember-cli-addon-docs/).
