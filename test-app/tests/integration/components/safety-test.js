import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { BASIC_CLASS, ARIA_LABELS } from 'ember-safe-button/components/safety';
import { SAFETY_STATUS } from 'ember-safe-button/components/safe-button';
import sinon from 'sinon';

module('Integration | Component | safety', function (hooks) {
  setupRenderingTest(hooks);

  hooks.beforeEach(function () {
    this.set('message', 'foo');
    this.set('safetyClass', 'bar');
    this.set('confirmSpy', sinon.spy());
    this.set('ariaLabel', 'Deactivate safety'); // TODO: make this configurable with fallback
    this.set('safetyStatus', SAFETY_STATUS.LOCKED);
    this.set('webAnimations', { unlocking: () => {}, locking: () => {} });
  });

  test('basic functionality', async function (assert) {
    await render(
      hbs`<Safety @safetyStatus={{this.safetyStatus}} @onClick={{this.confirmSpy}} @displayedMessage={{this.message}} class={{this.safetyClass}} />`
    );

    assert
      .dom('button')
      .hasText(this.message)
      .hasAttribute('type', 'button')
      .hasAria('label', this.ariaLabel)
      .hasClass(this.safetyClass)
      .hasClass(BASIC_CLASS);

    await click('button');

    assert.ok(this.confirmSpy.calledOnce);
  });

  test('safety states', async function (assert) {
    await render(
      hbs`<Safety @onClick={{this.confirmSpy}} @safetyStatus={{this.safetyStatus}} @webAnimations={{this.webAnimations}} />`
    );

    assert
      .dom('button')
      .isNotDisabled()
      .hasAttribute('tabindex', '0')
      .hasAttribute('aria-label', ARIA_LABELS[SAFETY_STATUS.LOCKED]);

    this.set('safetyStatus', SAFETY_STATUS.UNLOCKING);
    assert
      .dom('button')
      .isNotDisabled()
      .hasAttribute('tabindex', '0')
      .hasAttribute('aria-label', ARIA_LABELS[SAFETY_STATUS.UNLOCKING]);

    this.set('safetyStatus', SAFETY_STATUS.UNLOCKED);
    assert
      .dom('button')
      .isDisabled()
      .hasAttribute('tabindex', '-1')
      .hasAttribute('aria-label', ARIA_LABELS[SAFETY_STATUS.UNLOCKED]);

    this.set('safetyStatus', SAFETY_STATUS.LOCKING);
    assert
      .dom('button')
      .isNotDisabled()
      .hasAttribute('tabindex', '0')
      .hasAttribute('aria-label', ARIA_LABELS[SAFETY_STATUS.LOCKING]);
  });

  test('block usage', async function (assert) {
    await render(hbs`<Safety @onClick={{this.confirmSpy}}>bar</Safety>`);
    assert.dom('button').hasText('bar');
  });
});
