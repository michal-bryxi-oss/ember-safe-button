import Component from '@glimmer/component';

import { SAFETY_STATUS } from '../safe-button/index';

export const BASIC_CLASS = 'ember-safe-button-safety';
export const ARIA_LABELS = {
  [SAFETY_STATUS.LOCKED]: 'Deactivate safety',
  [SAFETY_STATUS.UNLOCKING]: 'Deactivating safety',
  [SAFETY_STATUS.UNLOCKED]: 'Safety not active',
  [SAFETY_STATUS.LOCKING]: 'Activating safety',
};

/**
  Safety is a component that covers the trigger.

  ```hbs
    <SafeButton
      @onConfirm={{action safeButtonClicked}} as |button|
    >
      <button.safety class="bg-grey-light">
        This is safety
      </button.safety>
    </SafeButton>
  ```

  @class SafetyComponent
 */
export default class SafetyComponent extends Component {
  BASIC_CLASS = BASIC_CLASS;

  get isDisabled() {
    return this.args.safetyStatus === SAFETY_STATUS.UNLOCKED;
  }

  get focusMe() {
    return !this.isDisabled;
  }

  get ariaLabel() {
    return ARIA_LABELS[this.args.safetyStatus];
  }
}
