import poing from './poing';
import slide from './slide';
import roll from './roll';
import flip from './flip';
import liftBars from './lift-bars';
import zoom from './zoom';

export default {
  poing,
  slide,
  flip,
  zoom,
  roll,
  'lift-bars': liftBars,
};
