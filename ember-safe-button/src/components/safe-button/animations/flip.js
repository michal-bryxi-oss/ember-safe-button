// TODO: https://github.com/animate-css/animate.css/blob/main/source/flippers/flipOutX.css
// TODO: https://github.com/animate-css/animate.css/blob/main/source/flippers/flipInX.css
export default {
  safety: {
    unlocking: {
      keyframes: [
        { transform: 'perspective(400px)', offset: 0 },
        {
          transform: 'perspective(400px) rotate3d(1, 0, 0, -20deg)',
          offset: 0.3 / 2,
          opacity: 1,
        },
        {
          transform: 'perspective(400px) rotate3d(1, 0, 0, 90deg)',
          offset: 1 / 2,
          opacity: 0,
        },
      ],
      options: {
        duration: 1000,
        fill: 'forwards',
        easing: 'ease-in',
      },
    },
    locking: {
      keyframes: [
        {
          transform: 'perspective(400px) rotate3d(1, 0, 0, 90deg)',
          offset: 0,
          opacity: 0,
        },
        {
          transform: ['perspective(400px)', 'rotate3d(1, 0, 0, -20deg)'],
          offset: 0.4 / 2 + 0.5,
        },
        {
          transform: ['perspective(400px)', 'rotate3d(1, 0, 0, 10deg)'],
          offset: 0.6 / 2 + 0.5,
          opacity: 1,
        },
        {
          transform: ['perspective(400px)', 'rotate3d(1, 0, 0, -5deg)'],
          offset: 0.8 / 2 + 0.5,
          opacity: 1,
        },
        {
          transform: 'perspective(400px)',
          offset: 1 / 2 + 0.5,
          opacity: 1,
        },
      ],
      options: {
        duration: 1000,
        fill: 'forwards',
        easing: 'ease-in',
      },
    },
  },
  trigger: {
    unlocking: {
      keyframes: [
        {
          transform: 'perspective(400px) rotate3d(1, 0, 0, 90deg)',
          offset: 0 + 0.5,
          opacity: 0,
        },
        {
          transform: 'perspective(400px) rotate3d(1, 0, 0, -20deg)',
          offset: 0.4 / 2 + 0.5,
        },
        {
          transform: 'perspective(400px) rotate3d(1, 0, 0, 10deg)',
          offset: 0.6 / 2 + 0.5,
          opacity: 1,
        },
        {
          transform: 'perspective(400px) rotate3d(1, 0, 0, -5deg)',
          offset: 0.8 / 2 + 0.5,
          opacity: 1,
        },
        {
          transform: 'perspective(400px)',
          offset: 1 / 2 + 0.5,
          opacity: 1,
        },
      ],
      options: {
        duration: 1000,
        fill: 'forwards',
        easing: 'ease-in',
      },
    },
    locking: {
      keyframes: [
        { transform: 'perspective(400px)', offset: 0 },
        {
          transform: 'perspective(400px) rotate3d(1, 0, 0, -20deg)',
          offset: 0.3 / 2,
          opacity: 1,
        },
        {
          transform: 'perspective(400px) rotate3d(1, 0, 0, 90deg)',
          offset: 1 / 2,
          opacity: 0,
        },
      ],
      options: {
        duration: 1000,
        fill: 'forwards',
        easing: 'ease-in',
      },
    },
  },
};
