export default {
  safety: {
    unlocking: {
      keyframes: [
        { transform: 'scale(1)', opacity: 1, offset: 0 },
        {
          transform: 'translateX(0px) scale(0.7)',
          opacity: 0.7,
          offset: 0.2,
        },
        {
          transform: 'translateX(-2000px) scale(0.7)',
          opacity: 0.7,
          offset: 1,
        },
      ],
      options: {
        duration: 2000,
        fill: 'both',
        easing: 'ease',
      },
    },
    locking: {
      keyframes: [
        {
          transform: 'translateX(-2000px) scale(0.7)',
          opacity: 0.7,
          offset: 0,
        },
        {
          transform: 'translateX(0px) scale(0.7)',
          opacity: 0.7,
          offset: 0.8,
        },
        { transform: 'scale(1)', opacity: 1, offset: 1 },
      ],
      options: {
        duration: 2000,
        fill: 'both',
        easing: 'ease',
      },
    },
  },
  trigger: {
    unlocking: {
      keyframes: [
        {
          opacity: 0,
          transform: 'translate3d(3000px, 0, 0) scaleX(3)',
          offset: 0,
        },
        {
          opacity: 1,
          transform: 'translate3d(-25px, 0, 0) scaleX(1)',
          offset: 0.6,
        },
        {
          opacity: 1,
          transform: 'translate3d(10px, 0, 0) scaleX(0.98)',
          offset: 0.75,
        },
        {
          opacity: 1,
          transform: 'translate3d(-5px, 0, 0) scaleX(0.995)',
          offset: 0.9,
        },
        {
          opacity: 1,
          transform: 'translate3d(0, 0, 0)',
          offset: 1,
        },
      ],
      options: {
        duration: 2000,
        fill: 'both',
        easing: 'cubic-bezier(0.215, 0.61, 0.355, 1)',
      },
    },
    locking: {
      keyframes: [
        {
          opacity: 1,
          transform: 'translate3d(-20px, 0, 0) scaleX(0.9)',
          offset: 0.2,
        },
        {
          opacity: 0,
          transform: 'translate3d(2000px, 0, 0) scaleX(2)',
          offset: 1,
        },
      ],
      options: {
        duration: 2000,
        fill: 'both',
        easing: 'ease',
      },
    },
  },
};
